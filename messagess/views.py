import json

from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from django.views import View
from django.views.generic import ListView, DetailView, CreateView, UpdateView
from django_filters.views import FilterView

from projects.models import Project
from utils.services import is_fan, remove_like, add_like
from .filters import MessageFilter
from .forms import AddOrEditMessageForm
from .models import Message, Tag
from utils.mixins import CreateOrUpdateMessageMixin, UpdateRequiredMixin


class MessageListView(LoginRequiredMixin, PermissionRequiredMixin, FilterView):
    permission_required = 'contenttypes.manager_perm'
    template_name = 'messagess/message_list.html'
    paginate_by = 10
    filterset_class = MessageFilter
    context_object_name = 'messages'
    queryset = Message.objects.select_related('project', 'project__company', 'manager')\
        .prefetch_related('tags', )


class MessageDetailView(LoginRequiredMixin, PermissionRequiredMixin, DetailView):
    permission_required = 'contenttypes.manager_perm'
    model = Message
    queryset = Message.objects.select_related('project', 'project__company', 'manager',)\
        .prefetch_related('tags')

    def get_context_data(self, *args, **kwargs):
        context = super().get_context_data(*args, **kwargs)
        context['liked'] = is_fan(self.object, self.request.user)
        return context


class MessageUpdateView(LoginRequiredMixin,
                        UpdateRequiredMixin,
                        PermissionRequiredMixin,
                        CreateOrUpdateMessageMixin,
                        UpdateView):
    permission_required = 'contenttypes.manager_perm'
    model = Message
    form_class = AddOrEditMessageForm
    queryset = Message.objects.select_related('project', 'project__company')

    def get_initial(self):
        tags = Tag.objects.filter(messagess__id=self.object.pk)
        self.initial.update({
            'new_tags': ' '.join([tag.name for tag in tags])
        })
        return super().get_initial()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Редактировать сообщение'
        context['update'] = True
        return context


class MessageCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateOrUpdateMessageMixin, CreateView):
    permission_required = 'contenttypes.manager_perm'
    model = Message
    form_class = AddOrEditMessageForm

    def get_initial(self):
        project = get_object_or_404(Project, pk=self.kwargs.get('project_pk'))
        self.initial.update({'project': project})
        self.initial.update({'manager': self.request.user})
        return super().get_initial()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'Создать сообщение'
        return context


class LikesView(LoginRequiredMixin, PermissionRequiredMixin, View):
    """
    Add or remove like to instance "Message"
    """
    permission_required = 'contenttypes.manager_perm'
    model = Message

    def post(self, request, pk):
        obj = get_object_or_404(self.model, pk=pk)
        if is_fan(obj, request.user):
            remove_like(obj, request.user)
            mdi_icon = 'mdi mdi-heart-outline'
        else:
            add_like(obj, request.user)
            mdi_icon = 'mdi mdi-heart'

        return HttpResponse(
            json.dumps({
                "like_count": self.model.objects.filter(pk=pk).values('grade').first()['grade'],
                "mdi_icon": mdi_icon,
            }),
            content_type="application/json"
        )
