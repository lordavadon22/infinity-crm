# Generated by Django 3.2.9 on 2021-12-01 15:27

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('companies', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('channel', models.CharField(choices=[('AP', 'Заявка'), ('LE', 'Письмо'), ('SI', 'Сайт'), ('CO', 'Инициатива компании')], default='AP', max_length=2, verbose_name='Канала обращения')),
                ('description', models.TextField(verbose_name='Описание сообщения')),
                ('grade', models.IntegerField(default=1, verbose_name='Оценка')),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='message_company', to='companies.company', verbose_name='Компания')),
            ],
            options={
                'verbose_name': 'Сообщения',
                'verbose_name_plural': 'Сообщений',
            },
        ),
    ]
