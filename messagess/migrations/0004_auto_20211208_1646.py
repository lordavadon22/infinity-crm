# Generated by Django 3.2.9 on 2021-12-08 14:46

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('messagess', '0003_auto_20211202_0938'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, verbose_name='Дата создания'),
        ),
        migrations.AlterField(
            model_name='message',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, verbose_name='Дата изменения'),
        ),
    ]
