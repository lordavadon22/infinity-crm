from ckeditor_uploader.fields import RichTextUploadingField
from django.conf import settings
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from django.db import models
from django.urls import reverse

from utils.base_models import TimestampedModel
from utils.const import RequestChannel


class Tag(TimestampedModel):
    """
    Creating 'Tag' models for tagging message from quick find
    """
    name = models.CharField(max_length=255, verbose_name='Тег')
    is_active = models.BooleanField(default=True)

    class Meta(TimestampedModel.Meta):
        verbose_name = "Тег"
        verbose_name_plural = "Теги"

    def __str__(self) -> str:
        """
        Returns a string representation of this `Tag`.
        """
        return self.name


class Message(TimestampedModel):
    """
    Create "Message" models for accounting of all interactions (correspondence, calls, mail)
     with the company for everyone project separately.
    """

    channel = models.CharField(
        max_length=2,
        choices=RequestChannel.choices,
        default=RequestChannel.APPLICATION,
        verbose_name='Канала обращения'
    )
    description = RichTextUploadingField()
    grade = models.IntegerField('Кол-во лайков', default=0)
    likes = GenericRelation('messagess.Like')
    project = models.ForeignKey('projects.Project', on_delete=models.CASCADE, related_name='message_project',
                                verbose_name='Проект компании')
    manager = models.ForeignKey('users.User', on_delete=models.CASCADE, related_name='message_manager',
                                verbose_name='Менеджер')
    tags = models.ManyToManyField(Tag, related_name='messagess', verbose_name='Теги')
    is_active = models.BooleanField(default=True)

    class Meta(TimestampedModel.Meta):
        verbose_name = "Сообщения"
        verbose_name_plural = "Сообщении"

    def __str__(self) -> str:
        """
        Returns a string representation of this `Message`.
        """
        return f'{self.project.company.name}:{self.project.name} - {RequestChannel(self.channel).label}'

    def get_absolute_url(self) -> str:
        """
        Returns absolute url of message
        """
        return reverse('messagess:message_detail', kwargs={'pk': self.pk})

    def get_verbose_channel_name(self):
        return RequestChannel(self.channel).label

    # @property
    # def is_liked(self):
    #     return self.likes.filter(user=user).exists()


class Like(TimestampedModel):
    """
    Create "Like" models for create/delete like from message
    """
    user = models.ForeignKey(settings.AUTH_USER_MODEL,
                             related_name='likes',
                             on_delete=models.CASCADE, verbose_name='Пользователь')
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, verbose_name='Модель')
    object_id = models.PositiveIntegerField(verbose_name='Id Модели')
    content_object = GenericForeignKey('content_type', 'object_id',)

    class Meta(TimestampedModel.Meta):
        verbose_name = "Лайк"
        verbose_name_plural = "Лайки"

    def __str__(self) -> str:
        """
        Returns a string representation of this `Like`.
        """
        return f'Like - {self.pk}'


