from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django import forms
from django.contrib import admin
from django.utils.safestring import mark_safe

from .models import Message, Tag, Like


@admin.register(Tag)
class TagAdmin(admin.ModelAdmin):
    list_display = ('name', 'is_active', 'created_at',)
    list_filter = ('created_at',)
    list_editable = ('is_active',)
    search_fields = ('name', 'created_at',)


class PostAdminForm(forms.ModelForm):
    description = forms.CharField(label='Описание', widget=CKEditorUploadingWidget())

    class Meta:
        model = Message
        fields = '__all__'


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = ('channel', 'show_description', 'project', 'grade', 'get_company', 'manager', 'is_active')
    list_filter = ('created_at',)
    list_editable = ('is_active',)
    search_fields = ('description', 'project', 'project', 'manager')
    form = PostAdminForm

    fieldsets = (
        ('Основная информация', {
            'fields': ('channel', 'description', 'grade', 'tags')
        }),
        ('Проект и компания', {
            'fields': ('project',)
        }),
        ('Обслуживающий персонал', {
            'fields': ('manager',)
        }),
        ('Разрешения',
         {'fields': ('is_active',)
          }),
    )
    add_fieldsets = (
        (None, {
            'fields': ('channel', 'description', 'grade', 'tags')
        }),
        ('Проект и компания', {
            'fields': ('project',)
        }),
        ('Обслуживающий персонал', {
            'fields': ('manager',)
        }),
        ('Разрешения', {
            'fields': ('is_active',)
        }),
    )

    def get_company(self, instance):
        return instance.project.company.name

    get_company.short_description = 'Компания'

    def show_description(self, obj):
        return mark_safe(obj.description)

    show_description.short_description = 'Описание сообщения'


@admin.register(Like)
class LikeAdmin(admin.ModelAdmin):
    list_display = ('user', 'content_object', 'created_at')
    list_filter = ('created_at',)
    search_fields = ('user', 'content_object', 'created_at')
