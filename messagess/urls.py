"""infinity_crm URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path, include

from . import views

urlpatterns = [
    path('', views.MessageListView.as_view(), name='message_list'),
    path('message/', include([
        path('<int:pk>/', views.MessageDetailView.as_view(), name='message_detail'),
        path('<int:pk>/like/', views.LikesView.as_view(), name='message_like'),
        path('<int:project_pk>/create/', views.MessageCreateView.as_view(), name='message_create'),
        path('<int:pk>/edit/', views.MessageUpdateView.as_view(), name='message_edit'),
    ]))
]
