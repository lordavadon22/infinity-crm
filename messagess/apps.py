from django.apps import AppConfig


class MessagesConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'messagess'
    verbose_name = 'Сообщения'

    def ready(self):
        import messagess.signals
