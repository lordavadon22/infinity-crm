from cProfile import label

from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django import forms
from django.core import validators

from messagess.models import Message
from projects.models import Project


class AddOrEditMessageForm(forms.ModelForm):

    description = forms.CharField(label='Описание', widget=CKEditorUploadingWidget())
    new_tags = forms.CharField(label='Теги', widget=forms.TextInput(
        attrs={'class': 'form-control mb-2', 'placeholder': 'Введите список тегов через пробел'}))

    class Meta:
        model = Message
        fields = ('channel', 'description', 'new_tags', 'project', 'manager')
        widgets = {
            'channel': forms.Select(attrs={'class': 'form-control mb-2'}),
            'project': forms.HiddenInput(),
            'manager': forms.HiddenInput()
        }


class TagsCheckboxSelectMultiple(forms.CheckboxSelectMultiple):
    template_name = 'messagess/widgets/multiple_input.html'
