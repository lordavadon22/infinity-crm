from cProfile import label

import django_filters

from messagess.forms import TagsCheckboxSelectMultiple
from messagess.models import Message, Tag
from utils.const import CHOICES


class MessageFilter(django_filters.FilterSet):
    tags = django_filters.ModelMultipleChoiceFilter(queryset=Tag.objects.all(),
                                                    widget=TagsCheckboxSelectMultiple())

    ordering = django_filters.OrderingFilter(choices=CHOICES, required=True, empty_label=None, )

    class Meta:
        model = Message
        fields = ('channel', 'grade', 'project', 'manager', 'tags')
        order_by_field = 'channel'
