from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver

from messagess.models import Like


@receiver(post_save, sender=Like)
def add_like(sender, instance, *args, **kwargs):
    message = instance.content_object
    message.grade += 1
    message.save()


@receiver(post_delete, sender=Like)
def add_like(sender, instance, *args, **kwargs):
    message = instance.content_object
    message.grade -= 1
    message.save()