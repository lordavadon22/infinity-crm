from ckeditor_uploader.fields import RichTextUploadingField
from django.core.validators import RegexValidator
from django.db import models
from django.urls import reverse

from utils.base_models import TimestampedModel


class Phone(TimestampedModel):
    """
    Create "Phone" models whose store phones of company.
    """
    phone_number_regex = RegexValidator(regex=r"^\+380\d{3}\d{2}\d{2}\d{2}$",
                                        message='Введите корректный номер телефона (например, +380671234567).')
    phone_number = models.CharField(validators=[phone_number_regex], max_length=13, unique=True,
                                    verbose_name='Номер телефона')
    company = models.ForeignKey('companies.Company', on_delete=models.CASCADE, null=True, related_name='phones_company',
                                verbose_name='Компания')
    is_active = models.BooleanField(default=True, verbose_name='Активный?')

    class Meta(TimestampedModel.Meta):
        verbose_name = "Телефон компании"
        verbose_name_plural = "Телефоны компании"

    def __str__(self) -> str:
        """
        Returns a string representation of this `Phone`.
        """
        return self.phone_number


class Email(TimestampedModel):
    """
    Create "Email" models whose store emails of company.
    """
    email_regex = RegexValidator(regex=r"^\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,}\b$",
                                 message='Введите корректный email адрес (например, example@mail.com')
    email = models.CharField(validators=[email_regex], max_length=50, unique=True, verbose_name='Email')
    company = models.ForeignKey('companies.Company', on_delete=models.CASCADE, null=True, related_name='emails_company',
                                verbose_name='Компания')
    is_active = models.BooleanField(default=True, verbose_name='Активный?')

    class Meta(TimestampedModel.Meta):
        verbose_name = "Email компании"
        verbose_name_plural = "Email компании"

    def __str__(self) -> str:
        """
        Returns a string representation of this `Email`.
        """
        return self.email


class Company(TimestampedModel):
    """
    Create "Company" models.
    """
    name = models.CharField('Имя компании', max_length=50, unique=True, db_index=True)
    supervisor = models.CharField('ФИО руководителя', max_length=50)
    short_description = RichTextUploadingField()
    address = models.CharField('Адрес компании', max_length=500)
    is_active = models.BooleanField(default=True, verbose_name='Активный?')
    manager = models.ForeignKey('users.User', on_delete=models.CASCADE, related_name='companies')

    class Meta(TimestampedModel.Meta):
        verbose_name = "Компания(-ю)"
        verbose_name_plural = "Компании"

    def __str__(self) -> str:
        """
        Returns a string representation of this `Company`.
        """
        return self.name

    def get_absolute_url(self) -> str:
        """
        Returns absolute url of company
        """
        return reverse('companies:company_detail', kwargs={'pk': self.pk})
