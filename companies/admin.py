from django.contrib import admin
from django import forms

from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django.utils.safestring import mark_safe

from .models import Company, Phone, Email


@admin.register(Phone)
class PhoneAdmin(admin.ModelAdmin):
    list_display = ('phone_number', 'company', 'is_active')
    list_filter = ('created_at', )
    list_editable = ('is_active',)
    search_fields = ('phone_number', 'phone_company',)


@admin.register(Email)
class EmailAdmin(admin.ModelAdmin):
    list_display = ('email', 'company', 'is_active')
    list_filter = ('created_at', )
    list_editable = ('is_active',)
    search_fields = ('email', 'email_company',)


class PhoneInLine(admin.TabularInline):
    model = Phone
    extra = 0


class EmailInLine(admin.TabularInline):
    model = Email
    extra = 0


class PostAdminForm(forms.ModelForm):
    short_description = forms.CharField(label='Краткое описание', widget=CKEditorUploadingWidget())

    class Meta:
        model = Company
        fields = '__all__'


@admin.register(Company)
class CompanyAdmin(admin.ModelAdmin):
    list_display = ('name', 'supervisor', 'safe_description', 'address', 'is_active')
    list_filter = ('created_at', )
    list_editable = ('is_active',)
    search_fields = ('name', 'supervisor', 'address')
    readonly_fields = ('created_at', 'updated_at')
    inlines = [EmailInLine, PhoneInLine]
    form = PostAdminForm

    fieldsets = (
        (None, {'fields': ('name', 'supervisor', 'short_description', 'address')}),
        ('Обслуживание', {'fields': ('is_active', ('created_at', 'updated_at'), 'manager')}),
    )

    def safe_description(self, instance):
        return mark_safe(instance.short_description)

    safe_description.short_description = 'Краткое описание'
