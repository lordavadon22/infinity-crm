from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django import forms
from django.forms import inlineformset_factory

from companies.models import Company, Phone, Email


class AddOrEditCompanyForm(forms.ModelForm):
    short_description = forms.CharField(label='Краткое описание', widget=CKEditorUploadingWidget())

    class Meta:
        model = Company
        fields = ('name', 'supervisor', 'address', 'short_description',)
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control mb-2',
                                           'placeholder': 'Введите название компании'}),
            'supervisor': forms.TextInput(attrs={'class': 'form-control mb-2',
                                                 'placeholder': 'Введите ФИО руководителя'}),
            'address': forms.TextInput(attrs={'class': 'form-control mb-2',
                                              'placeholder': 'Введите адрес компании'}),
        }


EmailFormSet = inlineformset_factory(Company, Email, fields=('email',), can_delete=False, extra=2, widgets={
    'email': forms.TextInput(attrs={'class': 'form-control mb-2',
                                    'placeholder': 'Введите email'}),
})

PhoneFormSet = inlineformset_factory(Company, Phone, fields=('phone_number',), can_delete=False, extra=2, widgets={
    'phone_number': forms.TextInput(attrs={'class': 'form-control mb-2',
                                           'placeholder': 'Введите номер телефона'}),
})
