from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import (ListView,
                                  DetailView,
                                  UpdateView,
                                  CreateView)

from companies.forms import AddOrEditCompanyForm
from companies.models import Company
from messagess.models import Message
from utils.mixins import CreateOrUpdateCompanyMixin, OrderMixin, UpdateRequiredMixin
from django.contrib.auth.mixins import PermissionRequiredMixin


class CompanyListView(LoginRequiredMixin, OrderMixin, ListView):
    model = Company
    paginate_by = 10
    template_name = 'companies/company_list.html'
    context_object_name = 'companies'
    order = 'name_asc'


class CompanyDetailView(LoginRequiredMixin, DetailView):
    model = Company
    queryset = Company.objects.all().prefetch_related('emails_company', 'phones_company', 'projects_company')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['messagess'] = Message.objects.filter(project__company=self.kwargs.get('pk'))
        return context


class CompanyUpdateView(LoginRequiredMixin,
                        UpdateRequiredMixin,
                        PermissionRequiredMixin,
                        CreateOrUpdateCompanyMixin,
                        UpdateView):
    permission_required = 'contenttypes.manager_perm'
    model = Company
    form_class = AddOrEditCompanyForm
    queryset = Company.objects.all().prefetch_related('emails_company', 'phones_company')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'редактировать информацию о компании'
        context['update'] = True
        return context


class CompanyCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateOrUpdateCompanyMixin, CreateView):
    permission_required = 'contenttypes.manager_perm'
    model = Company
    form_class = AddOrEditCompanyForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'добавить новую  компанию'
        return context
