from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from django.utils.safestring import mark_safe
from django.contrib.auth.models import Permission


from .forms import CustomUserCreationForm, CustomUserChangeForm


@admin.register(get_user_model())
class UserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = get_user_model()
    list_display = ('username', 'email', 'first_name', 'last_name', 'is_active', 'is_staff', 'show_avatar')
    list_editable = ('is_active', 'is_staff',)
    list_filter = ('created_at',)
    readonly_fields = ('show_avatar',)
    fieldsets = (
        ('Основная информация', {'fields': ('email', 'username', 'first_name', 'last_name', 'password',)}),
        ('Фото', {'fields': (('avatar', 'show_avatar'),)}),
        ('Разрешения', {'fields': ('is_staff', 'is_active', 'groups')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'password1', 'password2', 'is_staff', 'is_active')}
        ),
    )
    search_fields = ('username', 'email', 'first_name', 'last_name')
    ordering = ('email',)

    def show_avatar(self, obj):
        if str(obj.avatar).startswith('http'):
            avatar = obj.avatar
        else:
            avatar = obj.avatar.url
        return mark_safe(f'<img src={avatar} width="50" height="50">')

    show_avatar.short_description = 'Аватар'


admin.site.register(Permission)
