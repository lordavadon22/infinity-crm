import order as order
from django.contrib.auth import get_user_model
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, PasswordChangeView
from django.shortcuts import redirect
from django.views.generic import DetailView, UpdateView
from django.contrib.auth.forms import UserCreationForm
from django.urls import reverse_lazy
from django.views import generic

from utils.mixins import ProfileMixin, OrderMixin
from utils.services import get_message_paginated, ordering
from .forms import LoginForm, SignupForm, EditProfileForm, EditPasswordForm


class ProfileDetailView(LoginRequiredMixin, ProfileMixin, DetailView):
    model = get_user_model()
    paginated_by = 5
    template_name = 'users/profile_detail.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        order = ordering(self.request)
        context['page_obj'] = get_message_paginated(self.request,
                                                    self.object,
                                                    order,
                                                    paginated_by=self.paginated_by)
        context['order'] = order
        return context


class ProfileUpdateView(LoginRequiredMixin, ProfileMixin, UpdateView):
    form_class = EditProfileForm
    model = get_user_model()
    template_name = 'users/profile_edit.html'
    success_url = reverse_lazy('users:profile_detail')


class PasswordUpdateView(LoginRequiredMixin, PasswordChangeView):
    template_name = 'users/password_change_form.html'
    success_url = reverse_lazy('users:profile_edit')
    form_class = EditPasswordForm


class UserLoginView(LoginView):
    template_name = 'accounts/login.html'
    form_class = LoginForm

    def dispatch(self, *args, **kwargs):
        if self.request.user.is_authenticated:
            return redirect('companies:company_list')
        return super().dispatch(*args, **kwargs)


class UserSignupView(generic.CreateView):
    form_class = SignupForm
    success_url = reverse_lazy('users:login')
    template_name = 'accounts/register.html'
