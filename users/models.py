from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.urls import reverse

from .managers import CrmUserManager

from utils.base_models import TimestampedModel


class User(AbstractBaseUser, PermissionsMixin, TimestampedModel):
    """
    Create "User" models for manager's, client's and other.
    """
    username = models.CharField(db_index=True, max_length=50, unique=True, verbose_name='Имя пользователя')
    email = models.EmailField(db_index=True, unique=True, verbose_name='Email пользователя')
    first_name = models.CharField(max_length=50, verbose_name='Имя')
    last_name = models.CharField(max_length=50, verbose_name='Фамилия')
    is_active = models.BooleanField(default=True, verbose_name='Статус')
    is_staff = models.BooleanField(default=False, verbose_name='Обслуживающий персонал?')
    avatar = models.ImageField(upload_to='profiles', max_length=1000, verbose_name='Фотография пользователя',
                                      default='https://img2.freepng.ru/20180216/buq/kisspng-anonymous-icon-business-user-cliparts-5a8748c3513464.8197930015188154273326.jpg')

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    objects = CrmUserManager()

    class Meta(TimestampedModel.Meta):
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"

    def __str__(self) -> str:
        """
        Returns a string representation of this `User`.
        This string is used when a `User` is printed in the console.
        """
        return self.email

    def get_full_name(self) -> str:
        """
        This method is required by Django for things like handling emails.
        Typically, this would be the user's first and last name.
        """
        return f'{self.first_name} {self.last_name}'

    def get_short_name(self) -> str:
        """
        This method is required by Django for things like handling emails.
        Typically, this would be the user's first name.
        """
        return self.first_name

    def get_absolute_url(self) -> str:
        """
        Returns absolute url of user profile
        """
        return reverse('users:profile_detail', kwargs={'pk': self.pk})

    @property
    def show_avatar(self):
        if str(self.avatar).startswith('http'):
            avatar = self.avatar
        else:
            avatar = self.avatar.url
        return avatar
