// Лайк/дизлайк отдельного сообщения для рейтинга
function like() {
    var like = $(this);
    var pk = like.data('id');

    $.ajax({
        url: "like/",
        type: 'POST',
        data: {"obj": Number(pk)},

        success: function (json) {
            like.find("[data-count='like']").text(json.like_count);
            like.find("i.mdi").removeClass().addClass(json.mdi_icon)
        }
    });

    return false;
}

// Подключение обработчиков
$(function () {
    $('[data-action="like"]').click(like);
});

// Получение переменной cookie по имени
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

// Настройка AJAX
$(function () {
    $.ajaxSetup({
        headers: {"X-CSRFToken": getCookie("csrftoken")}
    });
});
