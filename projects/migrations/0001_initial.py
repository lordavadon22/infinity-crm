# Generated by Django 3.2.9 on 2021-12-01 15:27

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('companies', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Project',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('name', models.CharField(db_index=True, max_length=100, verbose_name='Имя проекта')),
                ('description', models.TextField(verbose_name='Описание проекта')),
                ('project_start', models.DateTimeField(verbose_name='Сроки начала проекта')),
                ('project_stop', models.DateTimeField(verbose_name='Сроки окончания проекта')),
                ('price', models.IntegerField(verbose_name='Стоимость проекта')),
                ('company', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='project_company', to='companies.company', verbose_name='Компания')),
            ],
            options={
                'verbose_name': 'Проект',
                'verbose_name_plural': 'Проекты',
            },
        ),
    ]
