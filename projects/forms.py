from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django import forms
from django.core.exceptions import ValidationError

from tempus_dominus.widgets import DateTimePicker

from companies.models import Company
from projects.models import Project


class CheckDateProject:
    def clean(self):
        cleaned_data = super().clean()
        project_start = cleaned_data['project_start']
        project_stop = cleaned_data['project_stop']

        if project_start > project_stop:
            raise ValidationError(
               'Дата окончания проекта не может быть раньше даты старта проекта'
            )



class FormMixin(forms.ModelForm):
    description = forms.CharField(label='Описание', widget=CKEditorUploadingWidget())
    project_start = forms.DateTimeField(label='Дата старта проекта',
                                        widget=DateTimePicker(
                                            options={
                                                'useCurrent': True,
                                                'collapse': False,
                                            },
                                            attrs={
                                                'append': 'fa fa-calendar',
                                                'icon_toggle': True,
                                                'class': 'form-control'
                                            }
                                        ),
                                        )
    project_stop = forms.DateTimeField(label='Дата окончания проекта',
                                       widget=DateTimePicker(
                                           options={
                                               'useCurrent': True,
                                               'collapse': False,
                                           },
                                           attrs={
                                               'append': 'fa fa-calendar',
                                               'icon_toggle': True,
                                               'class': 'form-control'
                                           }
                                       ),
                                       )


class AddOrEditProjectForm(CheckDateProject, FormMixin):

    class Meta:
        model = Project
        fields = ('name', 'description', 'price', 'project_start', 'project_stop', 'company')
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control mb-2',
                                           'placeholder': 'Введите название проекта'}),
            'price': forms.TextInput(attrs={'class': 'form-control mb-2',
                                            'placeholder': 'Введите цену проекта'}),
            'company': forms.HiddenInput()
        }
