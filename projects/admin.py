from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django import forms
from django.contrib import admin

from .models import Project


class PostAdminForm(forms.ModelForm):
    description = forms.CharField(label='Описание', widget=CKEditorUploadingWidget())

    class Meta:
        model = Project
        fields = '__all__'


@admin.register(Project)
class ProjectAdmin(admin.ModelAdmin):
    list_display = ('name', 'project_start', 'project_stop', 'price', 'is_active')
    list_filter = ('created_at', )
    list_editable = ('is_active',)
    search_fields = ('name', )
    form = PostAdminForm
    fieldsets = (
        (None, {'fields': ('name', 'description')}),
        (None, {'fields': ('price', 'company', 'manager')}),
        ('Срок реализации проекта', {'fields': (('project_start', 'project_stop'),)}),
        ('Статус', {'fields': ('is_active',)}),
    )
