from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.shortcuts import get_object_or_404
from django.views.generic import ListView, DetailView, CreateView, UpdateView

from companies.models import Company
from projects.forms import AddOrEditProjectForm
from projects.models import Project
from utils.mixins import OrderMixin, UpdateRequiredMixin, CreateOrUpdateProjectMixin


class ProjectListView(LoginRequiredMixin, OrderMixin, ListView):
    model = Project
    paginate_by = 10
    template_name = 'projects/project_list.htmls'
    context_object_name = 'projects'
    order = 'name_asc'
    queryset = Project.objects.select_related('company')


class ProjectDetailView(LoginRequiredMixin, DetailView):
    model = Project
    queryset = Project.objects.select_related('company').prefetch_related('message_project')


class ProjectUpdateView(LoginRequiredMixin,
                        UpdateRequiredMixin,
                        PermissionRequiredMixin,
                        CreateOrUpdateProjectMixin,
                        UpdateView):
    permission_required = 'contenttypes.manager_perm'
    model = Project
    form_class = AddOrEditProjectForm
    queryset = Project.objects.select_related('company').all()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'редактировать информацию о проекте'
        context['update'] = True
        return context


class ProjectCreateView(LoginRequiredMixin, PermissionRequiredMixin, CreateOrUpdateProjectMixin, CreateView):
    permission_required = 'contenttypes.manager_perm'
    model = Project
    form_class = AddOrEditProjectForm

    def get_initial(self):
        company = get_object_or_404(Company, pk=self.kwargs.get('pk'))
        self.initial.update({'company': company})
        return super().get_initial()

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['title'] = 'добавить новый проект'
        return context
