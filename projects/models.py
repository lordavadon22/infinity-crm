from django.db import models
from django.urls import reverse

from ckeditor_uploader.fields import RichTextUploadingField

from utils.base_models import TimestampedModel


class Project(TimestampedModel):
    """
    Create "Project" models for accounting of completed projects for companies.
    """
    name = models.CharField('Имя проекта', max_length=100, db_index=True)
    description = RichTextUploadingField()
    project_start = models.DateTimeField(verbose_name='Старт проекта')
    project_stop = models.DateTimeField(verbose_name='Окончание проекта')
    price = models.IntegerField('Стоимость проекта')
    company = models.ForeignKey('companies.Company', on_delete=models.SET_NULL, null=True,
                                related_name='projects_company',
                                verbose_name='Компания')
    is_active = models.BooleanField(default=True, verbose_name='Активный?')
    manager = models.ForeignKey('users.User', on_delete=models.CASCADE, related_name='projects')

    class Meta(TimestampedModel.Meta):
        verbose_name = "Проект"
        verbose_name_plural = "Проекты"

    def __str__(self) -> str:
        """
        Returns a string representation of this `Project`.
        """
        return f'{self.name} - {self.company.name}'

    def get_absolute_url(self) -> str:
        """
        Returns absolute url of project
        """
        return reverse('projects:project_detail', kwargs={'pk': self.pk})
