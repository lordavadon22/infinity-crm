from django.db import models


class RequestChannel(models.TextChoices):
    APPLICATION = 'AP', 'Заявка'
    LETTER = 'LE', 'Письмо'
    SITE = 'SI', 'Сайт'
    COMPANY = 'CO', 'Инициатива компании'


CHOICES =[
        ["channel", "по каналу обращения"],
        ["grade", "низкий рейтинг сверху"],
        ["-grade", "высокий рейтинг сверху"],
        ["created_at", "самые старые записи"],
        ["-created_at", "самые свежие записи"],
]

object_sorting = {
    'name_asc': 'name',
    'name_desc': '-name',
    'data_asc': 'created_at',
    'data_desc': '-created_at',
    'data_start_asc': 'project_start',
    'data_start_desc': '-project_start',
    'data_stop_asc': 'project_stop',
    'data_stop_desc': '-project_stop',
    'project_asc': 'project',
    'project_desc': '-project',
    'company_asc': 'project__company',
    'company_desc': '-project__company',
}
