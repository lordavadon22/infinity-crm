from typing import List, Tuple

from django.contrib.auth import get_user_model
from django.contrib.contenttypes.models import ContentType
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage

from messagess.models import Like, Message
from .const import object_sorting

User = get_user_model()


def add_like(obj: Message, user: User) -> None:
    """
    Likes `obj`.
    """
    obj_type = ContentType.objects.get_for_model(obj)
    Like.objects.get_or_create(
        content_type=obj_type, object_id=obj.id, user=user)


def remove_like(obj: Message, user: User) -> None:
    """
    Dislake `obj`.
    """
    obj_type = ContentType.objects.get_for_model(obj)
    Like.objects.filter(
        content_type=obj_type, object_id=obj.id, user=user
    ).delete()


def is_fan(obj: Message, user: User) -> bool:
    """
    Check like `user` to `obj`.
    """
    obj_type = ContentType.objects.get_for_model(obj)
    likes = Like.objects.filter(
        content_type=obj_type, object_id=obj.id, user=user)
    return likes.exists()


def get_fans(obj: Message) -> List[User]:
    """
    Get all users, whose like `obj`.
    """
    obj_type = ContentType.objects.get_for_model(obj)
    return User.objects.filter(
        likes__content_type=obj_type, likes__object_id=obj.id)


def get_message_paginated(request, obj, order, paginated_by=5):
    message_list = Message.objects.filter(manager=obj).select_related('project', 'project__company').order_by(order)
    paginator = Paginator(message_list, paginated_by)
    page = request.GET.get('page')
    try:
        page_obj = paginator.page(page)
    except PageNotAnInteger:
        # Если страница не является целым числом, поставим первую страницу
        page_obj = paginator.page(1)
    except EmptyPage:
        # Если страница больше максимальной, доставить последнюю страницу результатов
        page_obj = paginator.page(paginator.num_pages)
    return page_obj


def ordering(request):
    order = request.GET.get('sorting', '-created_at')
    return object_sorting.get(order, '-created_at')
