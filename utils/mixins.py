from django.http import HttpResponseForbidden, HttpResponseRedirect
from django.shortcuts import redirect, get_object_or_404

from companies.forms import PhoneFormSet, EmailFormSet
from messagess.models import Tag
from utils.services import ordering


class CreateOrUpdateCompanyMixin:
    def form_valid(self, form):
        context = self.get_context_data(form=form)
        formsets = [context['phone_formset'], context['email_formset']]
        if all(list(map(lambda x: x.is_valid(), formsets))):
            self.object = form.save(commit=False)
            self.object.manager = self.request.user
            self.object.save()
            response = HttpResponseRedirect(self.get_success_url())
            for formset in formsets:
                instances = formset.save(commit=False)
                for instance in instances:
                    instance.company = self.object
                    instance.save()
        else:
            return super().form_invalid(form)
        return response

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        if self.request.POST:
            context['phone_formset'] = PhoneFormSet(self.request.POST, instance=self.object)
            context['phone_formset'].full_clean()
            context['email_formset'] = EmailFormSet(self.request.POST, instance=self.object)
            context['email_formset'].full_clean()
        else:
            context['phone_formset'] = PhoneFormSet(instance=self.object)
            context['email_formset'] = EmailFormSet(instance=self.object)
        return context


class UpdateRequiredMixin:
    def dispatch(self, request, *args, **kwargs):
        obj = self.model.objects.filter(pk=kwargs['pk']).first()
        if request.user.pk != obj.manager.pk:
            return HttpResponseForbidden()
        return super().dispatch(request, *args, **kwargs)


class OrderMixin:
    def get_queryset(self):
        self.order = ordering(self.request)
        return self.model.objects.all().order_by(self.order)

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['order'] = self.order
        return context


class CreateOrUpdateProjectMixin:
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.manager = self.request.user
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())


class CreateOrUpdateMessageMixin:
    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.manager = self.request.user
        self.object.save()
        self.object.tags.set(
            [Tag.objects.get_or_create(name=value)[0] for value in form.cleaned_data.get('new_tags').split()]
        )
        self.object.save()
        return HttpResponseRedirect(self.get_success_url())


class ProfileMixin:
    def get_object(self, queryset=None):
        if queryset is None:
            queryset = self.get_queryset()
        return queryset.filter(pk=self.request.user.pk).get()
